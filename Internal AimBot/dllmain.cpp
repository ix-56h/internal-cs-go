#include <windows.h>
//print
#include <iostream>
#include <inttypes.h>
//endprint
#include "internal.h"
#include "aimbot.h"

IClient eClient;
IEngine eEngine;

DWORD WINAPI internalMain(HMODULE hMod) {
#ifdef _DEBUG
    AllocConsole();
    FILE* f;
    freopen_s(&f, "CONOUT$", "w", stdout);
#endif 
    eEngine.getScreenSize();

    pEntity* closestEnemy;

    printf("entityList: %" PRIxPTR "\n", eClient.pEntityList);
    printf("localPlayer: %p\n", (const char*)eClient.localPlayer);
    printf("clientState : %" PRIxPTR "\n", eClient.pClientState);
    printf("ViewAngle : %" PRIxPTR "\n", (uintptr_t)(eEngine.viewAngle));
    printf("localPlayer.viewOffset : %" PRIxPTR "\n", (uintptr_t)(&eClient.localPlayer->vecViewOffset));
    printf("width : %i \nheight : %i\n", eEngine.width, eEngine.height);
    printf("maxPlayer : %i\n", eClient.maxPlayers);
    printf("%p\n", (const char *)eClient.localPlayer + (uintptr_t)hazedumper::netvars::m_fFlags);
    bool aimbot = false;

    int i = 0, x = 0;

    uint32_t smooth = 13370;
    //while (!GetAsyncKeyState(VK_END))
    while (!GetAsyncKeyState(VK_ESCAPE))
    {
        if (GetAsyncKeyState(VK_DELETE))
        {
            if (aimbot == false)
                aimbot = true;
            else
                aimbot = false;
        }
        if (aimbot == true)
        {
            closestEnemy = getClosestEntity(eClient.pEntityList);
            if (closestEnemy != nullptr)
            {
                Vec3 dst = bones(closestEnemy, 2);
                //Vec3 dst = closestEnemy->vecOrigin;

                //printf("head.x = %f\nhead.y = %f\nhead.z = %f\n", dst.x, dst.y, dst.z);
                
                // HERE IF I LISTEN ALL BASTARD FROM INTERNET I NEED TO DO :
                Vec3 vecOrigin = eClient.localPlayer->vecOrigin;
                //Vec3 vecOrigin = bones(eClient.localPlayer, 2);
                //Vec3 src = eClient.localPlayer->vecOrigin + eClient.localPlayer->vecViewOffset;
                uint32_t flag = *(uint32_t*)((const char *)eClient.localPlayer + hazedumper::netvars::m_fFlags);
                //printf("flag = %p\n", ((const char*)eClient.localPlayer + hazedumper::netvars::m_fFlags));
                //printf("%i\n", flag);

                // Need to get the eye position
                if (flag & IN_DUCK)
                    vecOrigin.z += 44.f;
                else
                    vecOrigin.z += 64.f;

                Vec3 newAngle = calcAngle(vecOrigin, dst);
                Vec3 diff = newAngle - *eEngine.viewAngle;
                diff.Normalize();
                eEngine.viewAngle->x += diff.x / smooth;
                eEngine.viewAngle->y += diff.y / smooth;
                //eEngine.viewAngle->x = newAngle.x;
                //eEngine.viewAngle->y = newAngle.y;
            }
        }
    }
#ifdef _DEBUG
    if (f != nullptr) fclose(f);
    FreeConsole();
#endif
    FreeLibraryAndExitThread(hMod, 0);
    return 0;
}
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        // Create new thread
        HANDLE tHndl = CreateThread(nullptr, 0, (LPTHREAD_START_ROUTINE)internalMain, hModule, 0, 0);
        if (tHndl) CloseHandle(tHndl);
        else return FALSE;
        break;
    }
    return TRUE;
}