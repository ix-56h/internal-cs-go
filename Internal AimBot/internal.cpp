#include <windows.h>

uintptr_t GetModuleBaseAddress(const wchar_t* moduleName) {
	return (uintptr_t)GetModuleHandleW(moduleName);
}