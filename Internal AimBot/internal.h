#ifndef INTERNAL_H
#define INTERNAL_H
#include "offsets.h"
#include "vector.h"
#include "player.h"
#include "engine.h"
uintptr_t GetModuleBaseAddress(const wchar_t* moduleName);
Vec3 calcAngle(Vec3 src, Vec3 dst);
pEntity* getClosestEntity(uintptr_t entityList);
Vec3 bones(pEntity* ent, int boneID);

struct view_matrix_t {
	float matrix[16];
};
#endif