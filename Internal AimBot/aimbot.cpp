#include <inttypes.h>
#include <iostream>
#include <cmath>
#include "internal.h"
#include "aimbot.h"

#define PI 3.1415927f
extern IClient eClient;
extern IEngine eEngine;

struct boneMatrix_t {
    char pad3[12];
    float x; //we maake a struct for the bone matrix XYZ and pad it
    char pad1[12];
    float y;
    char pad2[12];
    float z;
};

Vec3 xbones(pEntity *ent, int bone)
{
    Vec3 bones;
    printf("%i\n", sizeof(boneMatrix_t));
    //printf("%i\n", ent->boneMatrix + (0x30 * bone) + 0x0C);
    bones.x = *(float*)(ent->boneMatrix + (0x30 * bone) + 0x0C);
    bones.y = *(float*)(ent->boneMatrix + (0x30 * bone) + 0x1C);
    bones.z = *(float*)(ent->boneMatrix + (0x30 * bone) + 0x2C);
    return bones;
}

Vec3 bones(pEntity* ent, int boneID)
{
    Vec3 bonePos;
    struct boneMatrix_t bonema;
    bonema = *(boneMatrix_t*)(ent->boneMatrix + (sizeof(boneMatrix_t) * boneID));
    bonePos.x = bonema.x;
    bonePos.y = bonema.y;
    bonePos.z = bonema.z;

    return bonePos;
}

Vec3 calcAngle(Vec3 src, Vec3 dst)
{
    Vec3 delta = dst - src;
    float hyp = delta.Length();
    float pitch = -asinf(delta.z / hyp) * (180 / PI);
    float yaw = atan2f(delta.y, delta.x) * (180 / PI);
    delta.z = 0;

    if (pitch >= -89 && pitch <= 89 && yaw >= -180 && yaw <= 180)
    {
        delta.x = pitch;
        delta.y = yaw;
    }
    return (delta);
}

Vec3 xcalcAngle(Vec3 src, Vec3 dst)
{
    Vec3 angles;
    Vec3 delta = src - dst;
    double hyp = delta.Length();
    angles.y = (atanf(delta.y / delta.x) * 57.295779513082f);
    angles.x = (atanf(delta.z / hyp) * 57.295779513082f);
    angles.z = 0.00;

    if (delta.x >= 0.0)
        angles.y += 180.0f;
    return angles;
}

float getDistance(Vec3 src, Vec3 dst)
{
    Vec3 delta = dst - src;

    return (delta.Length());
}

struct Vec3 WorldToScreen(Vec3 pos, struct view_matrix_t matrix) {
    Vec3 out;
    float _x = matrix.matrix[0] * pos.x + matrix.matrix[1] * pos.y + matrix.matrix[2] * pos.z + matrix.matrix[3];
    float _y = matrix.matrix[4] * pos.x + matrix.matrix[5] * pos.y + matrix.matrix[6] * pos.z + matrix.matrix[7];
    out.z = matrix.matrix[12] * pos.x + matrix.matrix[13] * pos.y + matrix.matrix[14] * pos.z + matrix.matrix[15];

    _x *= 1.f / out.z;
    _y *= 1.f / out.z;

    out.x = eEngine.width * .5f;
    out.y = eEngine.height * .5f;

    out.x += 0.5f * _x * eEngine.width + 0.5f;
    out.y -= 0.5f * _y * eEngine.height + 0.5f;

    return out;
}

bool betterTarget(Vec3 src, pEntity* entity, float* curClosestDistance, Vec3* curClosestAngle)
{
    //Vec3 dst = bones(closestEnemy, 2);

    //uint32_t flag = *(uint32_t*)((const char*)eClient.localPlayer + hazedumper::netvars::m_fFlags);
    //printf("flag = %p\n", ((const char*)eClient.localPlayer + hazedumper::netvars::m_fFlags));
    //printf("%i\n", flag);

    // Need to get the eye position
    //if (flag & IN_DUCK)
    //    vecOrigin.z += 44.f;
    //else
    //    vecOrigin.z += 64.f;

    Vec3 newAngle = calcAngle(src, entity->vecOrigin);
    Vec3 diff = newAngle - *eEngine.viewAngle;
    diff.Normalize();
    float dist = getDistance(src, entity->vecOrigin);
    if (((diff.y <= curClosestAngle->y && diff.y >= -(curClosestAngle->y))
        && (diff.x <= curClosestAngle->x && diff.x >= -(curClosestAngle->x)))
        || (dist < *curClosestDistance && (diff.y <= 7 && diff.y >= -7)
            && (diff.x <= 7 && diff.x >= -7))
       )
    {
        //if (dist < *curClosestDistance)
        //{
            *curClosestAngle = diff;
            *curClosestDistance = dist;
            return (true);
        //}
    }
    return false;
    //here we can check viewangle, distance, if walls etc
    //actually just check de distance between the Current closest enemy and another enemy
    
    
    //Vec3 screenPos = WorldToScreen(src, pClientState + hazedumper::signatures::dwViewMatrix);
    
    //REALLY NEED TO REFACTOR, can't access to clientstate width etc without global
    // this is not possible, tomorow just refacto and select target if closest to cursor with world2screen

    //return (getDistance(src, entity->vecOrigin) < curClosestDistance);
}

pEntity* getClosestEntity(uintptr_t entityList)
{
    uint32_t i = 0;
    pEntity* closestPtr = nullptr;
    float closestDistance = 1000;
    Vec3 closestAngle = {7, 7, 0};
    for (size_t i = 0; i < eClient.maxPlayers; i++)
    {
        pEntity* entity = *reinterpret_cast<pEntity**>(entityList + (i * 0x10));
        if (!entity)
            continue;
        if ((uintptr_t)entity == (uintptr_t)(eClient.localPlayer))
            continue;
        if (eClient.localPlayer->iHealth < 1 || entity->iHealth < 1)
            continue;
        if (eClient.localPlayer->iTeamNum == entity->iTeamNum)
            continue;
        if (!entity->bDormant && betterTarget(eClient.localPlayer->vecOrigin, entity, &closestDistance, &closestAngle))
            closestPtr = entity;
    }
    return (closestPtr);
}